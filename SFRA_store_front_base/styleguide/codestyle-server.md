[TOC]

## 1 Why having code conventions
Code conventions are important to programmers for a number of reasons:

* 80% of the lifetime cost of a piece of software goes to maintenance.
* Hardly any software is maintained for its whole life by the original author.
* Code conventions improve the readability and maintainability of the software, allowing engineers to understand new code more quickly and thoroughly.
* If you ship your source code as a product, you need to make sure it is as well packaged and clean as any other product you create.

For the conventions to work, every person writing software must conform to the code conventions.
**Everyone!**.

To help enforce certain code conventions it is useful to use tools such as eslint and JSCS which can help warn about non-adherence and automatically correct the formatting.

# 2. General Rules

## 2.1 Business Object definitions (Metadata)
There are two kinds of business objects: **system objects** and **custom objects**.

Both system and custom object attribute names start with a lowercase letter and are written in camel case, e.g.:

`displayName`

`articleNumber`

This applies to site and organization preferences as well!

The ID of a custom object type should be written in pascal casing – e.g.:

`OnlinePrintCatalog`

## 2.2 Project Structure

### 2.2.1 Cartridge Structure
In Commerce Cloud projects there is typically one Storefront (app) cartridge and one Business Manager (bm) cartridge.
These are prefixed by `app_` and `bm_`:

`app_myproject`

`bm_myproject`

For more complex projects there may be multiple storefront cartridges for different sites or it may be necessary to encapsulate specific application parts into additional cartridges, e.g.:

| Cartridge name | Content |
| -------------- | ------- |
| `ui_myproject` | static content for user interface design, e.g.: css, js and image files |
| `bc_myfoomodule` | business components, encapsulates larger pieces of business logic (like an integration layer, batch process layer etc.) |
| `int_mybarmodule` | integration cartridge, encapulates an integration into its own cartridges, typically used for LINK as well |

Cartridges may depend on each other, e.g. the storefront cartridge may depend on an integration cartridge.
This dependency has to be reflected in proper cartridge settings using cartridge properties.

### 2.2.2 Cartridge Content
Commerce Cloud cartridges encapsulate forms, pipelines, scripts, templates and resources.
Typically they are structured like the following:

* cartridge
   * forms
   * controllers
   * ~~pipelines~~
   * scripts
   * templates
      * resources

Both template and script files should be filed in logical units.
E.g. templates for user actions:

* templates
   * default
      * user
         * pt_account.isml
         * registration.isml
         * showprofile.isml
         * editprofile.isml

If needed, there can also be sub-directories, a directory should typically contain no more than 10 templates or scripts.

More rules for the directory structure of scripts and templates can be found in the following chapters three and four in this document.

## 2.3 File Names
File names should always give an idea of the semantic value of the specific file.
The used names should provide the possibility to easily distinguish between different files and their specific purpose.
The more accurate a file name is the easier it can be found and understood.

`calculateCart.js` beats `cart1.js`

`productreviewlist.isml` beats `reviews.isml`

All names of files do have a specific case style that is listed below.
Note that all file names but pipeline file names start with a lowercase letter. E.g.:

| File Type | Suffix | Naming Convention | Case |
| --------- | ------ | ----------------- | ---- |
| ISML Templates | .isml | productpreview.isml | lowercase |
| Form Definitions | .xml | tellafriend.xml | lowercase |
| Properties | .properties | foobar.properties | lowercase |
| Locale Properties | .properties | foobar_de.properties | lowercase |
| Controller | .js | ProductFeed.js | camel-casing |
| Pipeline Definitions | .xml | ProductFeed.xml | camel-casing |
| Script modules | .js | wishlist.js | lowercase |

## 2.4 A Note on Pipelines
**The use of pipelines should be considered as deprecated and is strongly discouraged!**

As a temporary exception pipelines can be used for jobs until the platform INtegration Framework supports import & export capabilities.
When pipelines are used for this purpose, they should be kept very lightweight. logic should be implemented in scripts as much as possible which can be re-used using the script based job processing.

# 3 Coding style

## 3.1 Source Code Formatting
A file consists of sections that should be separated by blank lines and an optional comment identifying each section.
Files longer than 3000 lines are cumbersome and should be avoided.
There are different ways of breaking it into pieces eg. like template includes, remote includes, script libraries, etc.

## 3.2 Whitespace

### 3.2.1 Indentation
Set soft tabs to **4 spaces**.
Trailing whitespaces should be avoided.

(eslint: indent jscs: validateIndentation)

### 3.2.2 Leading braces

Place **1 space** before the leading brace.

(eslint: space-before-blocks jscs: requireSpaceBeforeBlockStatements)

### 3.2.3 Blank Spaces
* A keyword followed by a parenthesis should be separated by a space.
Example:

```
while (tableIndex < 10) {
  ...
}
```

  Note that a blank space should not be used between a function name and its opening parenthesis.
  This helps to distinguish keywords from function calls.

* A blank space should appear after commas in argument lists

* All binary operators except `.` should be separated from their operands by spaces.
Blank spaces should never separate unary operators such as unary minus, increment `++`, and decrement `--` from their operands.
Example:

```
var totalObjectHeight = topHeight + bottomHeight;

var totalObjectWidth = (a + b) / (c * d);

while (fooVar <= ++someCount) {
    fooVar++;
}

prints("size is " + fooVar + "\n");
```

* The expressions in a for statement should be separated by blank spaces.
Example:

```
  for (expr1; expr2; expr3)
```


## 3.3 Line Length
Avoid lines longer than 120 characters.

Note: Examples for use in documentation should have a shorter line length – generally no more than 70 characters.

## 3.4	Encoding
Standard encoding for all files is UTF-8.

## 3.5	Line breaks
Default line break is unix style linefeed `\n`.

## 3.6	Comments
There are two kinds of comments: implementation comments and documentation comments.
Implementation comments are those found in C++, which are delimited by `/*...*/`, and //. Documentation comments (known as “doc comments”) are delimited by `/**...*/`.
Doc comments can be extracted to HTML files to document source code.

Implementation comments are means for commenting out code or for comments about the particular implementation.
Doc comments are meant to describe the specification of the code, from an implementation-free perspective to be read by developers who might not necessarily have the source code at hand.

The Build Suite uses JSDoc3 (https://github.com/jsdoc3/jsdoc) to generate documentation from scripts, for a full reference of possible tags, please check this page: http://usejsdoc.org/  

### 3.6.1 Documentation Comments
Doc comments describe functions.
Each doc comment is set inside the comment delimiters `/**...*/`, with one comment per member.
This comment should appear just before the declaration. It consists of two main parts.
First a general comment on the function and second – separated by a blank comment line – parameter and return descriptions in JSDoc style.

```
    /**
     * The example function provides ...
     *
     * @param {dw.catalog.Product} description of param goes here
     * @return {String} description of return value goes here
     */
    function example(product) {
    // My single line comment
    /*
    My block comment
    is very long
    */
    ...
    }
```

Note how the parameters are described by the `@param` statement and the `@return` value is described by `@returns`.
For more detailed information on the syntax see the JSDoc specification (http://usejsdoc.org/).
If you need to give information about a variable or function that isn’t appropriate for documentation, use an implementation block comment or single-line comment.
For example, details about the implementation of a function should go in such an implementation block comment following the function statement, not in the function doc comment.
If a block comment has multiple lines both comment begin `/*` and end `*/` statement are in their own new line and the content is indented one more level.
Doc comments are forbidden to be positioned inside a function block.

### 3.6.2 Special Comments
Use `TODO` and `FIXME` in a comment to flag something that is bogus but works.
Use `FIXME` to flag something that is bogus and broken.

### 3.6.3 Inline Comments
Inline Comments are not recommended. Code can become difficult to read if comments immediately follow the code on the same line.
Examples of bad commenting style:
```
var a = 1; // declaring a to 1

function getRandomNumber() {
    return 4; // chosen by fair dice roll.
              // guaranteed to be random.
}

/* A block comment before code */ var b = 2;

var c = 3; /* A block comment after code */
```
Examples of good commenting style:
```
// This is a comment above a line of code
var foo = 5;

var bar = 5;
// This is a comment below a line of code
```

## 3.7 Naming Conventions
Naming conventions make programs more understandable by making them easier to read.
They can also give information about the function of the identifier.

| Identifier Type | Rules for Naming | Examples |
| --------------- | ---------------- | -------- |
| Classes Namespaces | All identifiers are in mixed case with a capital first letter. Class names should not start with underscore _ or dollar sign $ characters, even though both are allowed. | NavigationHelper |
| Variables |	The variable name should be descriptive and lowercase	| var myName; |
| Functions	| All identifiers are in mixed case with a lowercase first letter. Internal words start with capital letters. Variable names should not start with underscore _ or dollar sign $ characters, even though both are allowed. |	getBackground(); |
| Exported Controller endpoints | All exported functions are in mixed case with a uppercase first letter. Internal words start with capital letters. Variable names should not start with underscore _ or dollar sign $ characters, even though both are allowed. |	exports.AddProduct = addProduct; |
| Other module exports | All exported functions are in camel case with a lowercase first letter. Variable names should not start with underscore _ or dollar sign $ characters, even though both are allowed. | exports.addProduct = addProduct; |

## 3.8 Declarations

### 3.8.1 Variables
Do not align the names for variables/objects.
Avoid assigning several variables to the same value in a single statement.
It is hard to read.
Example:

```
  fooBar.fooChar = barFoo.barChar = 'c'; 	/* AVOID! */
```

Do not use embedded assignments in an attempt to improve run-time performance.
This is the job of the compiler.
Example:

```
  /* AVOID! */
  d = (a = b + c) + r;

  /* GOOD! */
  a = b + c;
  d = a + r;
```

#### 3.8.1.1 Deprecated variable typing
Declaring new variables with a specific type has been deprecated.
Separating the identifier and a type by colon **must no longer** be used.
In other words do not use the following:

```
  /* AVOID! */
  var indentLevel : Number;
  var tableSize : Number;

  /* GOOD! */
  var indentLevel;
  var tableSize;
```

### 3.8.2 Blocks
A block is any code surrounded by curly braces `{` and `}`.
Example:

```
  {
    ...
  }
```
Note that the indentation level of the ending brace `}` is the same as the one of the corresponding starting brace `{`.
Content of blocks is indented one more level.

### 3.8.3 Function Declarations & General Placement
When coding functions the following formatting rules should be followed:

* No space between a function name and the parenthesis `(` starting its parameter list
* After the closing brace `)` of the parameter list the return type is defined.
  The return type is separated from the parameter list by a colon which is enclosed by blanks.
* Open brace `{` appears in the same line as the function signature and follows the type definition separated by a blank.
* Closing brace `}` starts a line by itself indented to match its corresponding opening statement, except when it is a null statement the `}` should appear immediately after the `{`.
  No other statements should follow in the same line.

Function declarations are separated by a blank line.
Put declarations only at the beginning of blocks.
Don’t wait to declare variables until their first use;
it can confuse the unwary programmer and hamper code portability within the scope.

```
  /**
  * function description goes here
  *
  * @param {String} bar description goes here
  * @returns description of the return goes here
  */
  function doFoo(bar) {
      var int1 = 0;  /* beginning of method block */

      if (condition) {
          var int2 = 0;  /* beginning of "if" block */
          ...
      }
  }
```

The one exception to the rule is indexes of for loops, which can be declared in the for statement:

```
for (var i = 0; i < maxLoops; i++) {
    ...
}
```

Note how the for statement is followed by a blank whereas function names are not.

Organize functions in a file according to the stepdown rule: higher level functions should be on top and lower levels below. It makes it natural to read the source code.

```
/* AVOID! */
// "I need the full name for something..."
function getFullName(user) { 
    var fullName =  user.firstName + ' ' + user.lastName;
    return fullName;
}

function renderEmailTemplate(user) {  
    // "oh, here"
    var fullName = getFullName(user);
    var emailTemplate = 'Dear ' + fullName + ', ...';
    return emailTemplate;
}


/* GOOD! */
function renderEmailTemplate(user) {  
    //  "I need the full name of the user"
    var fullName = getFullName(user);
    var emailTemplate = 'Dear ' + fullName + ', ...';
    return emailTemplate;
}

// "I use this for the email template rendering"
function getFullName(user) { 
    var fullName =  user.firstName + ' ' + user.lastName;
    return fullName;
}
```

## 3.9 Statements

### 3.9.1 Simple Statements
Each line should contain at most one statement.
Example:

```
  argv++;		/* Correct */
  argv++; argc--; 	/* AVOID! */
```

### 3.9.2 Compound Statements
Compound statements are statements that contain lists of statements enclosed in braces `{ statements }`.
See the following sections for examples.

* The enclosed statements should be indented one more level than the compound statement.
* The opening brace should be at the end of the line that begins the compound statement and be separated by a blank;
  the closing brace should begin a line and be indented to the beginning of the compound statement.
* Braces are used around all statements, even single statements, when they are part of a control structure, such as a *if-else* or *for* statement.
  This makes it easier to add statements without accidentally introducing bugs due to forgetting to add braces.

### 3.9.3 Return Statements
A return statement with a value should not use parentheses unless they make the return value more obvious in some way.
The return value should be returned by an explicit variable.
There should be as few return statements as possible.
The maximum of two return statements should not be exceeded.
Example:

```
  function getTableSize() {
      var returnValue;
      ...
      returnValue = (size  > -1) ? size : defaultSize
      return returnValue;
  }
```

Still there are of course exceptions allowed:
* If the return value is calculated just before the return statement, it does not have to be an explicit variable, since declaring a variable only for the return statement doesn't make sense.
  Example:

  ```
    function foo() {
      var obj = new FooObject();
      ...
      return obj.toString();
    }
  ```

* In functions with a try-catch statement it is allowed to use one return statement per block.
  Example:

  ```
    try {
    	...
    	return returnValue;
    } catch (e) {
    	...
    	return returnValue;
    }
  ```

As you can see try and catch statements are directly followed by a blank and the opening braces for the new block.

### 3.9.4 Conditional Expressions
The result of a conditional expression is already boolean, avoid

  ```
    return empty(x)?true:false;
  ```

which is the same as

  ```
    return empty(x);
  ```

Avoid using complex conditions to i.e. check for the string `true`

```
  var result = typeof(x) == ‘string’ && x == ’true’;
  return result;
```

using strict type comparison is much cleaner to read

```
  return x === ‘true’;
```

### 3.9.5 If, if-else, if else-if else Statements
The if-else class of statements should have the following form:

```
  if (condition) {
      statements;

  } else if (condition) {
      statements;

  } else {
      statements;
  }
```

**Note:**
if statements always use braces `{}`.
Avoid the following error-prone form:

```
  if (condition) /*AVOID! THIS OMITS THE BRACES{}!*/
    statement;
```

### 3.9.6 For Statements
A for statement should have the following form:

```
  for (initialization; condition; update) {
      statements;
  }
```

Empty for statements are forbidden.
If it seems to be necessary, the code has to be refactored.
When using the comma operator in the initialization or update clause of a for statement, avoid the complexity of using more than three variables.
If needed, use separate statements before the for loop (for the initialization clause) or at the end of the loop (for the update clause).

### 3.9.7 While Statements
A while statement should have the following form:

```
  while (condition) {
      statements;
  }
```

Empty while statements are forbidden.
If it seems to be necessary, the code has to be refactored.

### 3.9.8 Switch Statements
A switch statement should have the following form:

```
  switch (condition) {
      case ABC:
          statements;
          /* falls through */

      case DEF:
          statements;
          break;

      case XYZ:
          statements;
          break;

      default:
          statements;
  }
```

Every time a case falls through (doesn't include a break statement), add a comment where the break statement would normally be.
This is shown in the preceding code example with the `/* falls through */` comment.
The last case must be the default case which doesn't necessarily have to include a break.

### 3.9.9 Try-Catch Statements
A try-catch statement should have the following format:

```
  try {
      statements;
  } catch (exception) {
      statements;
  }
```

A try-catch statement may also be followed by finally, which executes regardless of whether or not the try block has completed successfully.

```
  try {
      statements;
  } catch (exception) {
      statements;
  } finally {
      statements;
  }
```

### 3.9.10 Ternary Expressions
Here are two acceptable ways to format ternary expressions:

```
  alpha = (aLongBooleanExpression) ? beta : gamma;

  alpha = (aLongBooleanExpression) 
          ? beta 
          : gamma;
```

Try to avoid ternary expression operators, if possible – in almost all cases other than the short form above it is difficult to read.

### 3.9.11 Parentheses
It is generally a good idea to use parentheses liberally in expressions involving mixed operators to avoid operator precedence problems.
Even if the operator precedence seems clear to you, it might not be to others – you shouldn't assume that other programmers know precedence as well as you do.

```
  if (a == b && c == d) 		/* AVOID! */
  if ((a == b)
      && (c == d)) 	    		/* USE */
```

The `&&` and the `||` concatenators are positioned in front of the expression and separated by a blank.
The indentation of following expression line is one level more as the surrounding `if` statement.
Especially with long expressions, it increases readability.

### 3.9.12 Expressions before `?` in the Conditional Operator
If an expression containing a binary operator appears before the `?` in the ternary `?:` operator, it should be parenthesized.
Example:

```
  (x >= 0) ? x : -x;
```

## 3.10 CommonJS
Commerce Cloud development has support for CommonJS (http://www.commonjs.org/).
Hence modules are available or can be defined to cleanly handle scope issues and assist the development of maintainable code.

### 3.10.1 legacy importScript and importPackage
`importScript(...)` and `importPackage` was used in legacy implementations and should be considered bad practice.
Instead, all needed modules should use the CommonJS syntax.
This is achieved using `require(...)`.
Example:

```
  /* AVOID! */
  importPackage( dw.web );
  var url = URLUtils.http('Home-Show').toString();

  /* GOOD! */
  var URLUtils = require('dw/web/URLUtils');
  var url = URLUtils.http('Home-Show').toString();
```

### 3.10.2 Exposing functions outside of a module
All functions written within a module are visible within the module only.
To make the function available to outside of the module, it is required to expose the function by the function to a variable and exporting it.
Example:

```
  /* internal function */
  function hiddenFunction() {
    ...
  };

  /* exported function */
  function exposedFunction() {
    ...
  };
  exports.ExposedFunction = exposedFunction;
```

### 3.10.3 Standard compliant Javascript
CommonJS modules can be unit tested using other CommonJS runtimes like i.e. nodejs, in order to be able to do this it is necessary to use standard compliant Javascript only.

 * no `for each(var i in x)`
 * no use of E4X
 * no `__defineGetter()__` and `__noSuchMethod()`

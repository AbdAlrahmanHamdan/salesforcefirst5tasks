# Frequently asked questions

## Preamble
As the time has shown, most general "Where should I start?" or "What do I need to get started?"-type questions asked in sfcc-community.slack.com, can be answered with a limited amount of complementary information. This information is currently nowhere to be found except in the heads of active devs, or scattered in documentation, Xchange posts and Salesforce support platform wiki entries.  
To centralize this information and compile it in a way that can be quickly referenced to provide a starting point for new devs pressing questions, this document has been proposed. If you find a question is missing, feel free to add it via pull request.
___
## Questions and Answers

### Rhino JS Engine
On server side, Salesforce Commerce Cloud uses the Mozilla Rhino JavaScript engine to translate the Javascript from developers to Java that can be understood by the server. Any server side JavaScript code is executed by Rhino.  
Also see this page in documentation for additional information: https://documentation.demandware.com/DOC1/index.jsp?topic=%2Fcom.demandware.dochelp%2FScriptProgramming%2FDemandwareJavaScript.html

#### What Rhino version does Salesforce Commerce Cloud use?
Currently, version 17R5 is used.

#### Can I change the Rhino version?
No, it is part of the infrastructure set up by Salesforce and cannot in any way be altered.

#### If the underlying architecture is based on Java, can't I bypass Rhino and use Java instead?
No, this is not possible. If in some edge cases it appears possible, this is a bug/hack that will very likely go away soon.

#### What ECMA/ES features can I use with the currently used Rhino version?
You can get a comprehensive list of ES5/6 features supported by Rhino v17R5.
Don't get excited by the mention of ES6 however, ES6 support is not even rudimentary.
https://mozilla.github.io/rhino/compat/engines.html

___
### Coding environments, IDEs etc.

### Can I develop with \<my-favorite-code-editor> editor/IDE?
Yes, in a modern Salesforce Commerce Cloud project, 98% of the time will be spent writing JavaScript. Thus, any editor that you can use to write code will work. Everything above that (syntax highlighting, linting, upload to sandbox, debugging etc.) depends on each individual application. The most used/known will be covered here briefly:

#### Microsoft's Visual Studio Code
_**Note:**_ **Recommended for new devs.** This is the most mature and complete setup to develop for Salesforce Commerce Cloud, today. It offers the most options and convenience features. Therefore it is recommended to use it.  
All-in-one extension containing (but not limited to): Syntax scheme for ISML and DS files, advanced debugging options, code upload to instance, multi-root workspaces, console-mode on instance to live-evaluate code on the server: https://github.com/SqrTT/prophet  
For inline class documentation and auto-complete features, you can clone and setup this: https://bitbucket.org/demandware/dw-api-types/src/master/ (Refer to the repository readme on how to do this)

#### Github's Atom
Syntax Scheme for ISML and DS files: https://github.com/kenwheeler/language-demandware  
Plugin for Instance Code Upload & Debugging: https://github.com/SqrTT/Bart  
_**Note:**_ As many devs have moved on to use Visual Studio Code, Atom is still a valid option used by some. However, active development of plugins has largely stopped.

#### Sublime Text 2/3 & TextMate
Syntax Scheme for ISML and DS files: https://github.com/sholsinger/Demandware  
 Plugin for Instance Code Upload & Debugging: https://bitbucket.org/demandware/entropy/src/master/  
_**Note:**_ As most devs have moved on to use more capable, integrated solutions, development of plugins for Sublime stopped around three years ago.

#### JetBrains's WebStorm
Currently, it seems no usable plugin for Webstorm. Pair it with `dwupload` to upload your files to the sandbox. It is usable for Salesforce Commerce Cloud development, if you don't work with ISML too heavily, since the lack of ISML-specific support/highlighting.

#### IntelliJ
*TBD*

___
### Using JS Frameworks/JS language flavors serverside

#### Can I use <any-JS-template-engine> with Salesforce Commerce Cloud?
Most likely, yes! Since the transition of Salesforce from proprietary DemandwareScript to pure JavaScript on the server side, many frameworks can be used.

**_Caution!_**  
While you can use most frameworks, it is important to understand that a NodeJS app can cache a template over the entire lifetime, while Salesforce Commerce Cloud's "script lifecycle" is bound to the request.  
This means that if template compilation/rendering is costly, it may impact performance of your shop in general.

#### Handlebars.js
_**TBD**_

#### Typescript, Coffee Script etc.

##### Server Side
In general, the code running on the Salesforce Commerce Cloud will be always JavaScript. Period. If you want to write something else locally while developing, you can do that. Just make sure in the end, valid JavaScript with the supported feature set is put on the server.

##### Client Side
For client side code, you will need to generate native JavaScript. Depending on the supported browsers of your project, you can use whatever version of JavaScript you please.

##### Tanspiling
So for both use cases, both server- and front-end, you need to transpile the code you wrote into JavaScript. Commonly this is done in the build step. 
The most used solution for this would be the Babel module: https://babeljs.io/

##### Typescript
_**TBD**_

##### Coffee Script
_**TBD**_
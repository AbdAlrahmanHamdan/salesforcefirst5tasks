# CSS formatting
CSS files are formatted in single-line style where each selector definition is placed in one line.
Multiple definitions belonging to the same element should be grouped together where multiple groups of definitions are separated by a blank line.

Example:

```
    /* recommendations */
    .recommendations .details{margin:0 0 2px 0; color:#FF0029}
    .recommendations a{text-decoration:underline}
    .recommendationLink a:hover{text-decoration:none}

    /* recommendations in layer */
    .layerPopup .recommendations2nd{border-top:0}
    .layerPopup .recommendations .col{width:188px; height:147px;}
```

# SASS/LESS formatting
Since every compiler is able to to things like minifying and transformation of shorthand expressions, vendor-specific prefixing, etc. there is no need to save space or trying to be clever in your SASS/LESS files.
The biggest goal should always be legibility. This saves headaches and prevents errors!
Use single-line comments only, because multi-line comments are mostly preserved by compilers, where you normally would want to remove them from the compiled CSS file.

The details of how you ensure legibility really comes down to preference alot. A easy legible setup could look like:

```
    // example section of imaginary page
    .content {
	    display: none;
	    position: absolute;
	    right: 0;
	    @include font-class(f6);

		.mini-header {
			margin: 10px 10px 30px;
		}

		.single-row {
			@include clearfix;
			margin-bottom: 40px;

			.item-image {
				float: left;
				width: 70px;
				margin-right: $gutter;
			}
        }
    }
```

## Managing scale
It is also recommended to split your SASS/LESS definitions by functions, pages or modules. So instead of a huge, thousands-of-lines single "style.scss" you should aim for a "\_homepage.scss", "\_cart.scss" , "\_footer.scss" and so on. In the end, combine them all in your "style.scss" using "@import"s.
This way you are enabling parallel development without constant risk of conflicting commits.

## Managing variables
You will inevitable use alot of different variables such as color-values, metrics, webfont-icons and/or sprite-sheet classes on your pages. Normally you'll want to make sure they are consistant troughout potentially tens of thousands of lines of stylesheet as well as easily maintainable.
Use a dedicated file like "colors.scss" with something similar to the following structure:

```
    // basic color variables
    $dark-grey:         #b4b4b4;
    $concrete-grey:	    #d7d6d2;
    $bahia-green:       #a8b017;
    $crimson-red:       #d21432;
    $selective-yellow:  #efb400;
    
    // functional color variables
        // basic
    $background:            $grey;
    $main-font:             $dark-grey;
        // buttons 
    $button-standard-bg:	$green;
    $button-standard-hover: lighten($button-standard-bg, 30%);
    $button-standard-font:	$white;
    $button-cta-bg: 		$yellow;
    $button-cta-hover:		lighten($button-cta-bg, 5%);
    $button-cta-font:		$white;
```

*Notice*: At the top block, the colors themselves are defined. You define what shade of red you want to have (or rather get the definition from your companys styleguide/designers), what "blue" looks like or how many shades of grey you'll have.
The second block will the biggest part of your "colors.scss": It contains mappings from functional variables of elements on your page to color-variables.

While at first all this may seem redundant, at second thought this provides you with the following benefits: Whenever you want to change how a CTA-button in your shop looks, you only change the mapping of the color without affecting any other elements style.
If you find that, during an overhaul of your shop's design, the variation of yellow changes, you can change the color-variable globally without the need to go over hundreds of individual values.

**Hint**: When using a lot of different hues/shades of the same base color, naming can really help. There are tools online that you can use to get the name of a color: http://www.color-blindness.com/color-name-hue/

The same is applicable to icons or metrics or anything else that is used troughout the project.
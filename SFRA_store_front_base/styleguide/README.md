# Salesforce Commerce Cloud Code Styleguide

This styleguide is meant to help the community by applying a defined set of standards to the development process.

## Using the styleguide

Create a `package.json` with the following contents

```
{
    "devDependencies" : {
        "eslint": "~4.18.2",
        "eslint-config-commercecloud" : "git@github.com:SalesforceCommerceCloud/styleguide.git"
    }
}
```

and a .eslintrc.json with

```
{
    "extends": "eslint-config-commercecloud",
    "rules": {
    }
}
```

## [Frontend Styleguide](codestyle-client.md)

**Contributions welcome!**

## [Serverside Styleguide](codestyle-server.md)

## Setup VSC for linting

Install ESLint (either globally or in each workspace, the former is a bit less painful) & the ESLint extension. Once done & the .eslintrc is present in the workspace you will see linting issues, the extension can be configured to fix the linting issues on save.

## Setup studio for linting

_TBC_

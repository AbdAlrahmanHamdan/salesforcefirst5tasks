'use strict';

var server = require('server');

server.get('Test', function (req, res, next){
    var id=req.querystring.id;
    var HookMgr = require('dw/system/HookMgr');
    var temp = HookMgr.callHook('dw.ocapi.shop.product.modifyGETResponse',
                                'modifyGETResponse'
                                ,id);

    res.json({
        product: temp
    });

    next();
});

module.exports = server.exports();

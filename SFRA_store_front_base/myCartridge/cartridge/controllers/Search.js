'use strict';
var server = require('server');
var Search = module.superModule;
server.extend(Search);

server.append('Show', function (req, res, next) {
    var ContentMgr = require('dw/content/ContentMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');
    var content = ContentMgr.getContent('category-products');
    var exchangeratesapi = require('*/cartridge/scripts/services/exchangeratesapi.js');
    var temp = exchangeratesapi.exchange().call();
    var response = JSON.parse(temp.object.text);
    var exchangeRates = null;
    var allCustomObjects = CustomObjectMgr.getAllCustomObjects('exchangeRates').asList();
    var keys = Object.keys(response.rates);

    for (var i = 0; i < 32; i++) {
        var key = keys[i];
        Transaction.begin();
        if (!allCustomObjects.contains(CustomObjectMgr.getCustomObject('exchangeRates', key)))
        {
            exchangeRates = CustomObjectMgr.createCustomObject('exchangeRates', key);
            exchangeRates.getCustom().currency = key;
            exchangeRates.getCustom().value = Number(response.rates[key]);
        }

        else {
            exchangeRates = CustomObjectMgr.getCustomObject('exchangeRates', key);
            exchangeRates.getCustom().value = Number(response.rates[key]);
        }
        Transaction.commit();
    }

    var siteCategory = CatalogMgr.getCategory(req.querystring.cgid);
    var categoryName = siteCategory.getDisplayName();
    var showJSON = JSON.parse(content.getCustom().showJSON.toString());
    var categoryProducts = [];
    var tempProduct;
    for (var productLooper in showJSON) {
        tempProduct = ProductMgr.getProduct(showJSON[productLooper].ID);
        if (tempProduct.getCategories().contains(siteCategory)) {
            categoryProducts.push(tempProduct);
        }
    }
    var view = {
        categoryProducts: categoryProducts,
        siteCategory: req.querystring.cgid,
        categoryName: categoryName
    };
    res.setViewData(view);

    
    next();
});

module.exports = server.exports();

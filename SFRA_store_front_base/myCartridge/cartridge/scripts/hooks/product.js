'use strict';

var Site = require ('dw/system/Site');
var ProductMgr = require ('dw/catalog/ProductMgr')
function modifyGETResponse(id){
    var siteName = Site.getCurrent().getName();
    var product = ProductMgr.getProduct(id);
    return product.getName() + " " + siteName;

}

exports.modifyGETResponse = modifyGETResponse;
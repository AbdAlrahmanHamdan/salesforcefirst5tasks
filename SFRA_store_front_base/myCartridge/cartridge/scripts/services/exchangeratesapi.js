'use strict';

const LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
/**
 * @type {dw.system.Status}
 */
const Status = require('dw/system/Status');

function exchange() {
    return LocalServiceRegistry.createService('custom_service',{
        createRequest: function (svc, params) {
            svc.setRequestMethod('GET');
        },
        parseResponse: function (svc, res) {
            return res;
        },
        mockCall: function (svc, params) {}
    });
}

exports.exchange = exchange;

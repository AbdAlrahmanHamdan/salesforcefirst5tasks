'use strict';
/**
 * @module jobs/fetchProducts
 */
function fetchJson() {
    var ProductMgr = require('dw/catalog/ProductMgr');
    var ContentMgr = require('dw/content/ContentMgr');
    var URLUtils = require('dw/web/URLUtils');
    var allProducts = ProductMgr.queryAllSiteProducts();
    var tempProduct = 0;
    var products = [];
    for (var i = 0; i < allProducts.getCount(); i++) {
        tempProduct = allProducts.next();
        products.push({
            ID: tempProduct.getID(),
            ProductLink: URLUtils.https('Product-Show', 'pid', tempProduct.getID()).toString()
        });
    }
    allProducts.close();
    var contant = ContentMgr.getContent('category-products');
    contant.getCustom().showJSON = JSON.stringify(products);
}

exports.fetchJson = fetchJson;

'use strict';
var server = require('server');
var checkoutServices = module.superModule;
server.extend(checkoutServices);

server.append('PlaceOrder', function (req, res, next) {
    var Order = require('dw/order/OrderMgr');
    var Transaction = require('dw/system/Transaction');
    var viewData = res.getViewData();
    var currentOrder = Order.getOrder(viewData.orderID);

    Transaction.wrap(function () {
        var counter = currentOrder.getAllProductLineItems().size();

        currentOrder.custom.ProductCount = counter;
    });
    next();
});

module.exports = server.exports();

'use strict';

var server = require('server');
var URLUtils = require('dw/web/URLUtils');

server.get('Show', function (req, res, next) {
    var actionUrl = URLUtils.url('MyController-handleForm');
    res.render('product/page1', {
        actionUrl: actionUrl
    });
    next();
});

server.post('handleForm', function (req, res, next) {
    var number = req.form.number;
    var OrderMgr = require('dw/order/OrderMgr');
    var Transaction = require('dw/system/Transaction');
    var order = OrderMgr.getOrder(number);
    var orderStatus = order.getStatus();
    var orderTotalPrice = order.getTotalNetPrice();
    var orderCount = order.getCustom().myCounter == null ? 0 : order.getCustom().myCounter; // order.getCustom().myCounter || 0
    Transaction.wrap(function () {
        order.custom.myCounter = orderCount + 1;
    });
    orderCount = order.getCustom().myCounter;
    res.render('product/page2', {
        // number: number
        orderStatus: orderStatus,
        orderTotalPrice: orderTotalPrice,
        orderCount: orderCount
    });
    next();
});

module.exports = server.exports();

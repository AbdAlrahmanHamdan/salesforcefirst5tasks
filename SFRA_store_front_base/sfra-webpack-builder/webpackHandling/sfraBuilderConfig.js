'use strict';
const path = require('path');
/**
 * Allows to configure aliases for you require loading
 */
module.exports.aliasConfig = {
    // enter all aliases to configure
    alias : {
        base: path.resolve(
            process.cwd(), // eslint-disable-next-line max-len
            '../storefront-reference-architecture/cartridges/app_storefront_base/cartridge/client/default/'
        ),
		variBase: path.resolve(
            process.cwd(), // eslint-disable-next-line max-len
            '../app_vari/cartridges/app_vari/cartridge/client/default/'
        ),
		sourceDir: path.resolve(
            process.cwd(), // eslint-disable-next-line max-len
            '../storefront-reference-architecture/'
			),
		sfraPath: path.resolve(
            process.cwd(), // eslint-disable-next-line max-len
            '../storefront-reference-architecture/node_modules/'
			)
    }
}
// Name of you SFRA project folder
module.exports.sfraFolder = 'storefront-reference-architecture';
/**
 * Exposes cartridges included in the project
 */
module.exports.cartridges = [
'../storefront-reference-architecture/cartridges/app_storefront_base',
'../plugin_productcompare/cartridges/plugin_productcompare',
];